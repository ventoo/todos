app = require('derby').createApp(module)
  .use(require '../../ui/index.coffee')
_ = require 'lodash'


# CONSTANTS #

ENTER_KEY = 13
ESC_KEY = 27


# SCOPED FUNCTIONS #

processRequest = (page, model, params, next, callback) ->
  userId = model.get '_session.userId'
  itemsQuery = model.query 'todos', {userId}

  model.subscribe itemsQuery, (err) ->
    return next err if err

    todos = itemsQuery.ref '_page.todos'

    todos.filter('completedItems').ref '_page.completed'
    todos.filter('remainingItems').ref '_page.remaining'
    model.start 'toggleAllComplete', '_page.isAllCompleted', '_page.remaining'
    callback page, model


# ROUTES #

app.get '/', (args...) ->
  processRequest args.concat([(page, model) ->
      page.render 'todos', {itemsKey: 'todos'}
    ]
  )...

app.get '/active', (args...) ->
  processRequest args.concat([(page, model) ->
      page.render 'todos', {itemsKey: 'remaining'}
    ]
  )...

app.get '/completed', (args...) ->
  processRequest args.concat([(page, model) ->
      page.render 'todos', {itemsKey: 'completed'}
    ]
  )...


# CONTROLLER FUNCTIONS #

app.fn 'todo.add', ->
  item = @model.del '_page.newItem'

  return unless item

  item.userId = @model.get '_session.userId'
  item.isCompleted = false

  @model.at('todos').add item

app.fn 'todo.destroy', (e) ->
  item = e.get ':item'

  @model.at('todos').del item.id

app.fn 'edit.start', (e) ->
  item = e.get ':item'

  @model.set '_page.editingId', item.id, ->
    _.delay ->
      input = document.querySelector('li.editing .edit')

      input.focus() if input
    , 150

app.fn 'edit.process', (e, el) ->
  switch e.keyCode
    when ENTER_KEY, ESC_KEY
      @model.del '_page.editingId'
      if e.keyCode is ENTER_KEY
        item = e.get ':item'

        @model.at('todos').setEach item.id, {text: el.value}


app.fn 'edit.update', (e, el) ->
  editingId = @model.del '_page.editingId'

  return unless editingId

  item = e.get ':item'

  @model.at('todos').setEach item.id, {text: el.value}

app.fn 'completed.clear', ->
  todos = @model.at 'todos'
  completed = _.clone @model.get '_page.completed'

  todos.del item.id for item in completed

app.fn 'todos.toggleAllComplete', (e, el) ->
  todos = @model.at 'todos'
  items = _.clone @model.get '_page.todos'
  checked = el.checked

  todos.setEach item.id, {isCompleted: checked} for item in items


# MODEL FUNCTIONS #

app.on 'model', (model) ->
  model.fn 'completedItems', (item) ->
    item and item.isCompleted

  model.fn 'remainingItems', (item) ->
    item and not item.isCompleted

  model.fn 'toggleAllComplete', (remaining) ->
    return remaining and not remaining.length
